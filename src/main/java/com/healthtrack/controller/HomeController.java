package com.healthtrack.controller;

import com.healthtrack.entity.PhysicalActivity;
import com.healthtrack.service.PhysicalActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@Controller
public class HomeController {

    @Autowired
    private PhysicalActivityService physicalActivityService;

    @GetMapping("/home")
    public ModelAndView home() {
        List<PhysicalActivity> physicalActivities = physicalActivityService.getAll();
        return new ModelAndView("home", "physicalActivities", physicalActivities);
    }

    @PostMapping(value = "/home")
    public ModelAndView save(
            @RequestParam("name") String name,
            @RequestParam("minutes") int minutes,
            @RequestParam("calorieExpenditure") int calorieExpenditure) {

        List<PhysicalActivity> physicalActivities = physicalActivityService.save(name, minutes, calorieExpenditure);

        return new ModelAndView("home", "physicalActivities", physicalActivities);
    }
}
