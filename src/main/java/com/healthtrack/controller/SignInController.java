package com.healthtrack.controller;

import com.healthtrack.service.SignInService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SignInController {

    @Autowired
    private SignInService signInService;

    @GetMapping("/")
    public String index() {
        return "signIn";
    }

    @PostMapping(value = "/signin", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String signIn(@RequestParam("email") String email, @RequestParam("password") String password) {
        boolean isValidCredentials = signInService.isValidCredentials(email, password);

        if (!isValidCredentials) return "signIn";

        return "home";
    }
}
