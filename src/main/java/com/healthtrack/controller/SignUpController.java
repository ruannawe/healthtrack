package com.healthtrack.controller;

import com.healthtrack.service.SignUpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SignUpController {

    @Autowired
    private SignUpService signUpService;

    @GetMapping("/signup")
    public String index() {
        return "signUp";
    }

    @PostMapping(value = "/signup", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String save(
            @RequestParam("name") String name,
            @RequestParam("email") String email,
            @RequestParam("height") Double height,
            @RequestParam("password") String password) {

        signUpService.save(name, email, height, password);

        return "home";
    }
}
