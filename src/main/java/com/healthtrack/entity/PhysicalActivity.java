package com.healthtrack.entity;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name="physical_activities")
public class PhysicalActivity {

    @Id
    @GeneratedValue
    @Column(name="id")
    private long id;

    @Column(name="name")
    private String name;

    @Column(name="minutes")
    private int minutes;

    @Column(name="calorieExpenditure")
    private int calorieExpenditure;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "user_id", nullable = false)
    private User user;

    @Column(name="created_at")
    private LocalDateTime createdAt;

    @Column(name="updated_at")
    private LocalDateTime updatedAt;

    public PhysicalActivity() {}

    public PhysicalActivity(String name, int minutes, int calorieExpenditure, User user) {
        this.name = name;
        this.minutes = minutes;
        this.calorieExpenditure = calorieExpenditure;
        this.user = user;
        this.createdAt = LocalDateTime.now();
        this.updatedAt = LocalDateTime.now();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }

    public int getCalorieExpenditure() {
        return calorieExpenditure;
    }

    public void setCalorieExpenditure(int calorieExpenditure) {
        this.calorieExpenditure = calorieExpenditure;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public LocalDateTime getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(LocalDateTime updatedAt) {
        this.updatedAt = updatedAt;
    }
}