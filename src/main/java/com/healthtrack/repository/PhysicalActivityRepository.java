package com.healthtrack.repository;

import com.healthtrack.entity.PhysicalActivity;
import com.healthtrack.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PhysicalActivityRepository extends JpaRepository<PhysicalActivity, Long> {
    List<PhysicalActivity> findAllByUserByOrderByCreatedAtDesc(User user);
}
