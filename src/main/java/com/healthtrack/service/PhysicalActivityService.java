package com.healthtrack.service;

import com.healthtrack.entity.PhysicalActivity;
import com.healthtrack.entity.User;
import com.healthtrack.repository.PhysicalActivityRepository;
import com.healthtrack.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PhysicalActivityService {

    @Autowired
    private PhysicalActivityRepository physicalActivityRepository;

    @Autowired
    private UserRepository userRepository;

    public List<PhysicalActivity> getAll() {
        List<User> users = userRepository.findAll();
        User user = users.get(users.size() - 1);
        return physicalActivityRepository.findAllByUserByOrderByCreatedAtDesc(user);
    }

    public List<PhysicalActivity> save(String name, int minutes, int calorieExpenditure) {
        List<User> users = userRepository.findAll();
        User user = users.get(users.size() - 1);

        PhysicalActivity physicalActivity = new PhysicalActivity(name, minutes, calorieExpenditure, user);
        physicalActivityRepository.save(physicalActivity);

        return getAll();
    }
}
