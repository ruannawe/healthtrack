package com.healthtrack.service;

import com.healthtrack.entity.User;
import com.healthtrack.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SignInService {

    @Autowired
    private UserRepository userRepository;

    public Boolean isValidCredentials(String email, String password) {
        User user = userRepository.findByEmail(email);

        if (user == null) return false;

        return user.getPassword().equals(password);
    }
}